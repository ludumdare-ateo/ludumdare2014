﻿using UnityEngine;
using System.Collections;

public class CharScript : MonoBehaviour {
	
	public int control = 10;
	public bool isVulnerable;
	private bool isScared;

	// Use this for initialization
	void Start () {
		InvokeRepeating("updateControl",0,1f);
		
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log(control);
		if(!isVulnerable){
			if(Input.anyKeyDown && !Input.GetMouseButtonDown(0) && !Input.GetMouseButtonDown(1) && !Input.GetKeyDown("space") ){
				
				GameObject.Find("Tele").GetComponent<TVScript>().switchChannel();
				resetControl();
			}
			if(Input.GetKeyDown("space")){
				GameObject.Find("Tele").GetComponent<TVScript>().switchState();
			}
			
			if(Input.GetMouseButtonDown(0)){
				GameObject.Find ("Lamp").GetComponent<LampScript>().switchToggle();
			}
		}

		Color c = GameObject.Find ("KidShadow").renderer.material.color;
		if (GameObject.Find ("Lamp").GetComponent<LampScript> ().isOn ())
		    GameObject.Find ("KidShadow").renderer.material.color = new Color (c.r, c.g, c.b, 1f);
		else
			GameObject.Find ("KidShadow").renderer.material.color = new Color (c.r, c.g, c.b, 0f);
	}
	
	
	public void resetControl(){
		control = 20;
	}
	
	void updateControl(){
		
		isScared = true;
		
		if(GameObject.Find("Lamp").GetComponent<LampScript>().isOn())isScared = false;
		
		if(isScared){
			control--;
		}
		else{
			resetControl();
		}
		if(control <= 6)shake();
		if(control <= 0)soilHimself();
		
		
	}
	
	void shake(){
		
	}
	
	public void soilHimself(){
		if (isVulnerable) return;

		GameObject.Find ("Lamp").GetComponent<LampScript>().switchOff();
		GameObject.Find ("Tele").GetComponent<TVScript>().switchTVOff();
		isVulnerable = true;
	}
}
