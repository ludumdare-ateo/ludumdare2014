﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TVScript : MonoBehaviour {

	public Vector3 pointOne;
	public Vector3 pointTwo;
	public Vector3 pointThree;
	public Vector3 pointFour;

	public List<Texture> channels = new List<Texture>();
	private int activeChannelIndex;
	public AudioSource audio;

	public List<Texture> reflections = new List<Texture>();
	private Texture activeChannel;
	private bool tvIsOn;
	private GameObject screen;

	void Awake(){
		/*foreach(Texture channel in this.channels){
			MovieTexture movie = (MovieTexture) channel;
			AudioSource audio = this.gameObject.AddComponent("AudioSource") as AudioSource;
			audio.clip = movie.audioClip;
		}*/

		this.audio = this.gameObject.AddComponent("AudioSource") as AudioSource;
	}

	// Use this for initialization
	void Start () {
		screen = GameObject.Find("Screen");

		Mesh screenMesh = new Mesh();

		List<Vector3> vertices = new List<Vector3> ();
		List<int> triangles = new List<int> ();
		List<Vector2> screenUV = new List<Vector2> ();


		vertices.Add(this.pointOne);
		vertices.Add(this.pointTwo);
		vertices.Add(this.pointThree);
		vertices.Add(this.pointFour);



		triangles.Add(0);
		triangles.Add(1);
		triangles.Add(2);
		triangles.Add(0);
		triangles.Add(2);
		triangles.Add(3);

		screenUV.Add(new Vector2 (1.0f, 0.0f));
		screenUV.Add(new Vector2 (0.0f, 0.0f));

		screenUV.Add(new Vector2 (0.0f, 1.0f));
		screenUV.Add(new Vector2 (1.0f, 1.0f));




		screenMesh.vertices = vertices.ToArray();
		screenMesh.triangles = triangles.ToArray();
		screenMesh.uv = screenUV.ToArray();

		screenMesh.RecalculateBounds ();
		screenMesh.RecalculateNormals ();


		MeshFilter meshFilter = screen.GetComponent<MeshFilter> ();
		meshFilter.mesh = screenMesh;

		switchTVOn();

		InvokeRepeating("updateHours",0,1);
	}
	
	// Update is called once per frame
	void Update () {
		int rand = (int)Random.Range(9,11);
		if(rand == 10 && tvIsOn){blinkOnce();}
	}

	public void switchState(){
		if(tvIsOn)switchTVOff();
		else switchTVOn();
	}


	public void switchChannel(){
		if(tvIsOn){
			this.audio.Stop();
			this.activeChannelIndex = Random.Range (0,channels.Count);
			this.activeChannel = this.channels[this.activeChannelIndex];
			MovieTexture movie = (MovieTexture) activeChannel;
			this.audio.clip = movie.audioClip;
			screen.GetComponent<ScreenScript>().setChannel(movie, this.audio);
		}

	}

	public void switchTVOn(){
		tvIsOn = true;
		screen.GetComponent<ScreenScript>().switchOn();
		switchChannel();
		transform.FindChild("ScreenLight").GetComponent<Light>().enabled = true;

	}

	public void switchTVOff(){
		this.audio.Stop();
		tvIsOn = false;
		screen.GetComponent<ScreenScript>().switchOff();
		StopCoroutine("blink");
		transform.FindChild("ScreenLight").GetComponent<Light>().enabled = false;


	}

	public bool isOn(){
		return tvIsOn;
	}

	public void updateHours(){
		GameObject hours = GameObject.Find("Hours");
		
		TimeScript time = GameObject.Find("SkyLight").GetComponent<TimeScript>();
		hours.GetComponent<TextMesh>().text = GameObject.Find("SkyLight").GetComponent<TimeScript>().timeToString(time.minutes%2 == 0 ? " " : ":");
	}

	public void sizzle(){
		screen.GetComponent<ScreenScript>().sizzle();
	}

	void blinkOnce(){
		StartCoroutine(blink());
	}

	public IEnumerator blink(){

			// transform.GetComponent<AudioSource>().Play();
			transform.FindChild("ScreenLight").GetComponent<Light>().intensity = 7f;
			yield return new WaitForSeconds(0.08f);
			transform.FindChild("ScreenLight").GetComponent<Light>().intensity = 8f;

		
	}



}
