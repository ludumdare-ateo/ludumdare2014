﻿using UnityEngine;
using System.Collections;

public class LampScript : MonoBehaviour {
	public bool startState;
	public float startPower;
	public float powerLostBySecond;
	public float powerLostWhenSpeedSwitch;
	public float lightDurationShortTime;
	public float lightDurationLongTime;
	public float explodeTime;
	public float sizzleTime;

	private float currentExplodeTime;
	private float currentSizzleTime;
	private bool exploding;
	private bool sizzling;

	private Light light;
	private bool on;
	private bool broken;
	private float power;

	private float lightDuration;
	private float luminosity;
	private float intensityModifExplode;

	void Awake(){
		this.broken = false;
		this.lightDuration = 0.0f;
		this.luminosity = 0.0f;
		this.currentExplodeTime = 0.0f;
		this.exploding = false;
	}

	// Use this for initialization
	void Start () {
		this.on = this.startState;
		this.power = this.startPower;
		this.light = transform.GetComponentInChildren<Light>();
	}

	// Update is called once per frame
	void Update () {
		if (this.makeLight () && !this.exploding) {
			this.luminosity = 1.0f;
			this.lightDuration += Time.deltaTime;
			this.power -= (this.powerLostBySecond * Time.deltaTime);

			if (this.power <= 0.0f) {
					this.explode ();
			}
			else if (this.sizzling) {
				this.luminosity = 0.2f ;
				this.currentSizzleTime -= Time.deltaTime ;
				this.intensityModifExplode = Random.value;
				
				if (this.currentSizzleTime <= 0) {
					this.sizzling = false ;
					this.currentSizzleTime = this.sizzleTime ;
					this.intensityModifExplode = 0.0f ;
				}
			}

			Color c = GameObject.Find ("LampShadow").renderer.material.color;
			GameObject.Find ("LampShadow").renderer.material.color = new Color (c.r, c.g, c.b, 0f);
		} else if (this.exploding) {
			this.luminosity = 1.0f;
			this.currentExplodeTime += Time.deltaTime;

			this.intensityModifExplode = 6.0f * this.currentExplodeTime / this.explodeTime;

			if (this.currentExplodeTime >= this.explodeTime) {
					this.exploding = false;
					this.broken = true;
					this.intensityModifExplode = 6.0f;
			}
		}
		else{
			this.luminosity = 0.0f;
			this.intensityModifExplode = 0.0f;

			Color c = GameObject.Find ("LampShadow").renderer.material.color;
			GameObject.Find ("LampShadow").renderer.material.color = new Color (c.r, c.g, c.b, 1f);
		}
		

		/*Update appareance*/
		this.light.intensity = this.luminosity * 2.0f + this.intensityModifExplode;
	}

	public void blink(){
	}

	public void sizzle(){
		this.sizzling = true;
	}

	public void explode(){
		if (!this.isBroken () && !this.exploding) {
			this.lightDuration = 0.0f;
			this.exploding = true;
			GameObject.Find("Monster").GetComponent<MonsterScript>().moveToStart();
		}
	}

	public bool isOn(){
		return this.on;
	}

	public bool isBroken(){
		return this.broken;
	}

	public void switchToggle(){
		if (this.isOn ()) {
			this.switchOff();
		}
		else {
			this.switchOn();
		}
	}

	public void switchOn(){
		this.on = true;
	}

	public void switchOff(){
		if (this.lightDurationIsShort ()) {
			this.power -= this.powerLostWhenSpeedSwitch;
		}
		this.lightDuration = 0.0f;
		this.on = false;
	}

	public bool makeLight(){
		return (this.isOn () && !this.isBroken ());
	}

	private string lightDurationType(){
		if (this.lightDuration < this.lightDurationShortTime) {
			return "short";
		}
		if (this.lightDuration >= this.lightDurationLongTime) {
			return "long";
		}

		return "medium";
	}

	public bool lightDurationIsShort(){
		return this.lightDurationType ().Equals("short");
	}

	public bool lightDurationIsMedium(){
		return this.lightDurationType ().Equals("medium");
	}

	public bool lightDurationIsLong(){
		return this.lightDurationType ().Equals("long");
	}
}
