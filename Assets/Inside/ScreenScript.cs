﻿using UnityEngine;
using System.Collections;

public class ScreenScript : MonoBehaviour {

	
	//States Reflection
	private Sprite activeReflection;
	private bool isOn;
	public MovieTexture noiseTexture;
	public GameObject screenReflexion;
	public GameObject screenGhost ;

	public Sprite reflection0;
	public Sprite reflection1;
	public Sprite reflection2;
	public Sprite reflection3;
	public Sprite reflection4;
	public Sprite reflection5;
	public Sprite reflection6;
	public Sprite reflection7;

	private AudioSource audioNoise;
	private AudioSource currentAudio;
	private MovieTexture currentMovie;

	private CharScript kid;

	private bool gameover;

	// Use this for initialization
	void Start () {
		this.kid = GameObject.Find ("Kid").GetComponent<CharScript> ();
		this.audioNoise = gameObject.AddComponent("AudioSource") as AudioSource;
		this.audioNoise.clip = this.noiseTexture.audioClip;
		this.audioNoise.loop = true;
		this.noiseTexture.loop = true;
		this.audioNoise.volume = 0;
		this.noiseTexture.Play();
		this.audioNoise.Play();

		this.screenReflexion = GameObject.Find ("ScreenReflection");
		this.screenGhost = GameObject.Find ("ScreenGhost");
	}
	
	// Update is called once per frame
	void Update () {
		if (this.gameover && !this.kid.isVulnerable) {
			this.kid.soilHimself();
		}
	}

	public void setChannel(MovieTexture movie, AudioSource audio){
		if(isOn){
			this.currentMovie.Pause();
		}
		isOn = true;

		movie.loop = true;
		audio.loop = true;

		this.currentAudio = audio;
		this.currentMovie = movie;

		StartCoroutine(loadAndPlayCurrentChannel());
	}

	IEnumerator loadAndPlayCurrentChannel(){
		while(!this.currentMovie.isReadyToPlay){
			yield return new WaitForSeconds(5.0f);
		}
		
		this.currentAudio.volume = 0;
		this.currentMovie.Play();
		this.currentAudio.Play();
		
		float timeToWait = Random.Range(0.5f, 1.5f);
		
		this.transitionChannelStart();
		
		yield return new WaitForSeconds(timeToWait);
		
		this.transitionChannelStop();
	}
	
	private void transitionChannelStart(){
		if (this.isOn) this.audioNoise.volume = 0.05f;
		else this.audioNoise.volume = 0.0f;
		gameObject.renderer.material.mainTexture = this.noiseTexture;
	}
	
	private void transitionChannelStop(){
		this.audioNoise.volume = 0;
		this.currentAudio.volume = 0.5f;
		if(!this.isOn){
			setReflectionMonster();
			// gameObject.renderer.material.mainTexture = activeReflection;
			if (!this.gameover) {
				gameObject.renderer.material.mainTexture = null;
			}
		}
		else{
			gameObject.renderer.material.mainTexture = this.currentMovie;
		}
	}

	public void switchOff(){
		isOn = false;
		this.audioNoise.volume = 0;
		setReflectionMonster();
		// gameObject.renderer.material.mainTexture = activeReflection;
		if (!this.gameover) {
			gameObject.renderer.material.mainTexture = null;
			this.screenReflexion.SetActive (true);
		}

	}

	public void switchOn(){
		this.screenReflexion.SetActive (false);
	}

	public void animate(){

	}

	public void setReflectionMonster(){
		if (!this.gameover) {
			MonsterScript monster = GameObject.Find("Monster").GetComponent<MonsterScript>();
			if(monster.isCurrently("out of house")) activeReflection = reflection0;
			if(monster.isCurrently("in house")) activeReflection = reflection1;
			if(monster.isCurrently("opening door")) activeReflection = reflection2;
			if(monster.isCurrently("in door")) activeReflection = reflection3;
			if(monster.isCurrently("in room corner")) activeReflection = reflection4;
			if(monster.isCurrently("in middle of room")) activeReflection = reflection5;
			if(monster.isCurrently("just behind the kid")) activeReflection = reflection6;

			this.screenReflexion.GetComponent<SpriteRenderer>().sprite = activeReflection;
			this.screenGhost.GetComponent<SpriteRenderer>().sprite = null;

			if (monster.isCurrently ("killing the kid")) {
				activeReflection = reflection7;

				GameObject.Find ("Thunderstorm").GetComponent<ThunderstormScript>().lightning() ;
				this.screenGhost.GetComponent<SpriteRenderer>().sprite = reflection7;
				this.transitionChannelStart() ;
				this.switchOn() ;
				this.gameover = true ;
			}
		}
		else{
			this.audioNoise.volume = 0.05f;
		}
	}

	public void sizzle(){
		StartCoroutine(blink());
	}
	
	IEnumerator blink(){
		float timeToWait = Random.Range(0.2f, 0.5f);
		
		this.transitionChannelStart();
		
		yield return new WaitForSeconds(timeToWait);
		
		this.transitionChannelStop();
		
	}
}
