﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class MonsterScript : MonoBehaviour {
	public float actionInterval;
	public int startMoveProbability ;
	public int empoweringByHour ;

	private LampScript lampToLook;
	private TVScript television;
	private CharScript kid;
	private TimeScript dayTime;
	private float minimumActionInterval;
	private int moveProbability;
	private MonsterPath path;
	private MonsterPath currentStep;
	private string previousState;
	private string _state;
	private bool previousLightDurationWasMedium;

	private AudioControllerScript soundPlayer;

	void Awake() {
		this.path = new MonsterPath ();
		this.currentStep = this.path;
		this.minimumActionInterval = 0.1f;
		this._state = this.previousState = this.getCurrentState();
		this.previousLightDurationWasMedium = false;
	}

	// Use this for initialization
	void Start () {
		this.soundPlayer = GameObject.Find("AudioController").GetComponent<AudioControllerScript> ();
		this.kid = GameObject.Find ("Kid").GetComponent<CharScript> ();
		this.dayTime = GameObject.Find ("SkyLight").GetComponent<TimeScript> ();
		this.lampToLook = GameObject.Find ("Lamp").GetComponent<LampScript> ();
		this.television = GameObject.Find ("Tele").GetComponent<TVScript> ();
		this.actionInterval = this.actionInterval < this.minimumActionInterval ? this.minimumActionInterval : this.actionInterval;
		InvokeRepeating ("doAction", this.actionInterval, this.actionInterval);
	}
	
	// Update is called once per frame
	void Update () {
		if (this.getCurrentState () == this._state) return;
		else this._state = this.getCurrentState ();

		// GameObject.Find ("Outside").GetComponent<NightScript> ().audio.volume = 1.0f;
		if (this.isCurrently ("out of house")) {
			return;
		}
		if (this.isCurrently ("in house")) {
			if (UnityEngine.Random.Range (0, 100) < 50) this.soundPlayer.playIfAvailable (this.soundPlayer.doorFar);
			return ;
		}
		if (this.isCurrently ("opening door")) {
			if (UnityEngine.Random.Range (0, 100) < 75) this.soundPlayer.playIfAvailable (this.soundPlayer.door);
			return ;
		}

		// GameObject.Find ("Outside").GetComponent<NightScript> ().audio.volume = 0f;
		if (this.isCurrently ("in door")) {
			return ;
		}

		if (this.isCurrently ("in room corner")) {
			return ;
		}
		if (this.isCurrently ("in middle of room")) {
			if (UnityEngine.Random.Range (0, 100) < 50) this.soundPlayer.playIfAvailable (this.soundPlayer.monster);
			return ;
		}
		if (this.isCurrently ("just behind the kid")) {
			return ;
		}
		if (this.isCurrently ("killing the kid")) {
			return ;
		}
	}
	
	public void doAction(){
		if (this.lookAtLamp (this.lampToLook).Equals ("noLight")) {
			this.isMoving();
		}
	}

	private void updateMoveProbability(){
		this.moveProbability = this.startMoveProbability + this.dayTime.hours * this.empoweringByHour;

		if(this.kid.isVulnerable){
			this.moveProbability = 85;
		}
	}

	private void isMoving(){
		this.updateMoveProbability();

		int randomInt = UnityEngine.Random.Range (0, 100);
		if (randomInt <= this.moveProbability) {
			this.moveForward();
		}

		/* Debug.Log (this.getCurrentState ());
		if(this.isCurrently("killing the kid")){
			Debug.Log("Game over");
		} */
	}

	public void moveTo(MonsterPath destination){
		if(!this.television.isOn() && !this.previousState.Equals(this.getCurrentState())){
			this.television.sizzle();
		}

		this.previousState = this.getCurrentState();
		this.currentStep = destination;
	}

	public void moveForward(int choice){
		this.moveTo(this.currentStep.getNextStep(choice));
	}

	public void moveForward(){
		this.moveForward (0);
	}

	public void moveBack(){
		this.moveTo(this.currentStep.getPreviousStep());
	}

	public void moveToStart(){
		this.moveTo(this.path);
	}

	public string getCurrentState(){
		return this.currentStep.getName();
	}

	public bool isCurrently(string state){
		return this.getCurrentState().Equals (state);
	}

	public string lookAtLamp(LampScript lamp){
		bool lightDurationIsMedium = lamp.lightDurationIsMedium ();
		if (!lightDurationIsMedium) {
			this.previousLightDurationWasMedium = false;
		}

		if (lamp.makeLight()) {
			if(lamp.lightDurationIsLong() && !this.isCurrently("out of house")){
				this.moveToStart();
			}
			else if(lightDurationIsMedium && !this.previousLightDurationWasMedium){
				this.moveBack();
				this.previousLightDurationWasMedium = true;
			}

			return "light";
		}

		return "noLight";
	}
}