﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonsterPath {
	private List <MonsterPath> nextSteps;
	private int positionIndex;
	private MonsterPath previousStep;
	private int deep;
	private string name;

	public MonsterPath(){
		this.deep = 0;
		this.previousStep = null;
		this.positionIndex = 0;

		this.initAccordingToDeep ();
	}

	public MonsterPath(MonsterPath fromStep){
		this.deep = fromStep.getDeep () + 1;
		this.previousStep = fromStep;
		this.positionIndex = 0;

		this.initAccordingToDeep();
	}

	private MonsterPath(MonsterPath fromStep, int position){
		this.deep = fromStep.getDeep () + 1;
		this.previousStep = fromStep;
		this.positionIndex = position;

		this.initAccordingToDeep();
	}

	private void addNextSteps(int numberOfSteps){
		for (int i = 0; i < numberOfSteps; i++) {
			this.nextSteps.Add(new MonsterPath(this, i));
		}
	}

	public int getDeep(){
		return this.deep;
	}

	public string getName(){
		return this.name;
	}

	public MonsterPath getNextStep(int choice){
		int numberOfSubStep = this.nextSteps.Count;
		if (choice >= numberOfSubStep) {
			choice = numberOfSubStep-1;
		}
		if (choice < 0) {
			return this;
		}
		return this.nextSteps[choice];
	}

	public MonsterPath getPreviousStep(){
		if (this.previousStep == null) {
			return this;
		}

		return this.previousStep;
	}

	/*On definit les differents chemins du monstre*/
	private void initAccordingToDeep(){
		this.nextSteps = new List<MonsterPath>();
		
		switch(this.deep){

		case 0:
			this.addNextSteps(1);
			this.name = "out of house";
			break;

		case 1:
			this.addNextSteps(1);
			this.name = "in house";
			break;

		case 2:
			this.addNextSteps(1);
			this.name = "opening door";
			break;
		
		case 3:
			this.addNextSteps(1);
			this.name = "in door";
			break;
		
		case 4:
			this.addNextSteps(1);
			this.name = "in room corner";
			break;
		
		case 5:
			this.addNextSteps(1);
			this.name = "in middle of room";
			break;
		
		case 6:
			this.addNextSteps(1);
			this.name = "just behind the kid";
			break;
		
		case 7:
			this.name = "killing the kid";
			break;
			
		default:
			this.name = this.previousStep.getName();
			break;
		}
	}
}
