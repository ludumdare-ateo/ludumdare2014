﻿using UnityEngine;
using System.Collections;

public class SkyLightScript : MonoBehaviour {

	TimeScript time ;
	int timeSunrise = 6*60 ;
	float minLight = 0.5f ;
	float maxLight = 8f ;
	float minX = -8 ;
	float maxX = 8 ;

	// Use this for initialization
	void Start ()
	{
		this.time = transform.GetComponent<TimeScript>() ;
	}
	
	// Update is called once per frame
	void Update ()
	{
		float itime = (float)this.time.timestamp / (float)this.time.timestampEnd ;
		Vector3 position = transform.GetComponent<Light> ().transform.position;
		position.x = this.minX + (this.maxX - this.minX) * itime;
		transform.GetComponent<Light> ().transform.position = position;

		if (this.time.timestamp < this.timeSunrise) return ;

		float ilight = 1.0f - (float)((float)this.time.timestampEnd - (float)this.time.timestamp) / ((float)this.time.timestampEnd - (float)this.timeSunrise) ;
		transform.GetComponent<Light>().intensity = this.minLight + (this.maxLight - this.minLight) * ilight;
	}
}
