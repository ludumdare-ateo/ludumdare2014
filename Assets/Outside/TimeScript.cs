﻿using UnityEngine;
using System.Collections;

public class TimeScript : MonoBehaviour {

	public int hourBegin = 0 ;
	public int hourEnd = 7 ;
	public int timestampEnd ;

	public int timestamp ;
	public int hours ;
	public int minutes ;

	private int ms ;

	// Use this for initialization
	void Start ()
	{
		this.hours = this.hourBegin ;
		this.minutes = 0 ;
		this.timestampEnd = this.hourEnd * 60;

		InvokeRepeating("increaseTime", 1.0f, 1.0f) ;
	}
	
	// Update is called once per frame
	void Update ()
	{

	}

	void increaseTime ()
	{
		if (++this.minutes >= 60) {
			this.hours++ ;
			this.minutes = 0 ;
		}
		
		if (this.hours >= 24) {
			this.hours = 0 ;
			this.timestamp = 0 ;
		}

		this.timestamp = this.hours * 60 + this.minutes;
	}

	public string timeToString (string delimiter)
	{
		string h = this.hours >= 10 ? "" + this.hours : "0" + this.hours,
			   m = this.minutes >= 10 ? "" + this.minutes : "0" + this.minutes ;

		return h+delimiter+m ;
	}
}
