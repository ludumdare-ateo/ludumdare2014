﻿using UnityEngine;
using System.Collections;

public class ThunderstormScript : MonoBehaviour {
	public float thunderInterval;
	public float lightningInterval;

	public int thunderProbability;
	public int lightningProbability;

	private Light thunderlight;
	private GameObject windowLight;
	private LampScript lamp;
	private TVScript television;

	private float intensity;
	private float thunderIntensity;
	private float thunderIntensityVariation;
	private TimeScript dayTime;

	private AudioControllerScript soundPlayer;

	// Use this for initialization
	void Start () {
		this.soundPlayer = GameObject.Find("AudioController").GetComponent<AudioControllerScript> ();
		this.windowLight = GameObject.Find("WindowLight");
		this.intensity = 0.0f;
		this.thunderlight = transform.GetComponentInChildren<Light>();
		this.lamp = GameObject.Find ("Lamp").GetComponent<LampScript> ();
		this.television = GameObject.Find ("Tele").GetComponent<TVScript> ();
		this.dayTime = GameObject.Find ("SkyLight").GetComponent<TimeScript> ();

		InvokeRepeating ("thunderHappening", this.thunderInterval, this.thunderInterval);
		InvokeRepeating ("lightningHappening", this.lightningInterval, this.lightningInterval);
	}
	
	// Update is called once per frame
	void Update () {
		float finalIntensity = this.intensity + this.thunderIntensity;
		this.thunderlight.intensity = finalIntensity >= 8.0f ? 8.0f : finalIntensity;

		float windowLightValue = (finalIntensity / 8.0f);
		Color c = this.windowLight.renderer.material.color;
		c.a = windowLightValue > 0.1f ? windowLightValue : 0.1f ;
		this.windowLight.transform.GetComponent<SpriteRenderer>().color = new Color(c.r, c.g, c.b, c.a);
		// this.windowLight.renderer.material.SetColor((int) (windowLightValue > 24 ? windowLightValue : 24));
	}

	private void thunderHappening(){
		int randomInt = UnityEngine.Random.Range (0, 100);
		if (randomInt <= this.thunderProbability) {
			this.thunder();
		}

		if (dayTime.hours > 5.0f) {
			CancelInvoke();
		}
	}

	private void lightningHappening(){
		int randomInt = UnityEngine.Random.Range (0, 100);
		if (randomInt <= this.lightningProbability) {
			this.lightning();
		}

		if (dayTime.hours > 5.0f) {
			CancelInvoke();
		}
	}

	public IEnumerator farThunder(){
		while (this.thunderIntensityVariation < 7) {
			this.thunderIntensity = Random.Range(1.0f, 4.0f);
			yield return new WaitForSeconds(Random.Range(0.06f, 0.15f));
			this.thunderIntensityVariation++;
		}
		this.thunderIntensity = 0;
	}

	public void thunder(){
		this.thunderIntensityVariation = 0;
		StartCoroutine("farThunder");

		float waitTime = Random.Range (0.6f, 1.3f);
		Invoke ("playThunderSound", waitTime);
	}

	private void playThunderSound(){
		int rand = Random.Range (0, 100);
		if (rand > 50) {
			this.soundPlayer.playIfAvailable(this.soundPlayer.thunder1);
		} else {
			this.soundPlayer.playIfAvailable(this.soundPlayer.thunder2);
		}
	}

	public void thunderSoundStrong(){
		this.soundPlayer.playIfAvailable(this.soundPlayer.thunder3);
	}

	public void lightning(){
		int rand = Random.Range (0, 100);
		if (rand > 40) {
			this.lamp.sizzle();
			this.television.sizzle();
		}

		this.intensity = 0.3f * 8.0f;
		Invoke ("lightningMiddle", UnityEngine.Random.Range (0.1f, 0.2f));
		Invoke ("thunderSoundStrong", UnityEngine.Random.Range (0.3f, 0.5f));
	}

	public void lightningMiddle(){
		this.intensity = 1.0f * 8.0f;
		Invoke ("lightningEnd", UnityEngine.Random.Range (0.2f, 0.3f));
	}

	private void lightningEnd(){
		this.intensity = 0.0f;
	}
}
