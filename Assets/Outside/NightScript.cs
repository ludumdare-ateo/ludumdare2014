﻿using UnityEngine;
using System.Collections;

public class NightScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		AudioControllerScript acs = GameObject.Find ("AudioController").GetComponent<AudioControllerScript> ();
		acs.playLoop (acs.night);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
