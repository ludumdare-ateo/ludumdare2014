﻿using UnityEngine;
using System.Collections;

public class AudioControllerScript : MonoBehaviour {
	
	public AudioSource door;
	public AudioSource doorFar;
	public AudioSource lightBubbleBreak;
	public AudioSource lightSizzle;
	public AudioSource morning;
	public AudioSource night;
	public AudioSource thunder1;
	public AudioSource thunder2;
	public AudioSource thunder3;
	public AudioSource monster;

	// Use this for initialization
	void Start () {
		/*door = GameObject.Find("Door").GetComponent<AudioSource>();
		doorFar = GameObject.Find("DoorFar").GetComponent<AudioSource>();
		lightBubbleBreak = GameObject.Find("LightBubbleBreak").GetComponent<AudioSource>();
		lightSizzle = GameObject.Find("Sizzle").GetComponent<AudioSource>();
		morning = GameObject.Find("Morning").GetComponent<AudioSource>();
		night = GameObject.Find("Night").GetComponent<AudioSource>();
		thunder1 = GameObject.Find("Thunder1").GetComponent<AudioSource>();
		thunder2 = GameObject.Find("Thunder2").GetComponent<AudioSource>();
		thunder3 = GameObject.Find("Thunder3").GetComponent<AudioSource>();*/
	}
	// Update is called once per frame
	void Update () {
	
	}

	public void playOnce(AudioSource source){
		source.loop = false;
		source.Play();
	}

	public void playIfAvailable(AudioSource source){
		if(!source.isPlaying){
			source.loop = false;
			source.Play();
		}
	}


	public void playLoop(AudioSource source){
		source.loop = true;
		source.Play();
	}

	public void stop(AudioSource source){
		source.Stop();
	}
	



}
